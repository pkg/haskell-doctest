module Imports (module Imports) where

import           Prelude as Imports
import           Data.Monoid as Imports
import           Data.Maybe as Imports
import           Control.Monad as Imports
import           Control.Arrow as Imports

pass :: Monad m => m ()
pass = return ()
